# myjobs

myjobs is a bbjobs (LSF) equivalent to monitor jobs in Slurm. It is based on the sacct and sstat command which is parsed and then the information is displayed in a human readable way. When running the command without specifying any jobid, then it will display information for all jobs of the user.

```
[sfux@eu-login-35 ~]$ myjobs -h
/cluster/apps/local/myjobs: Script to monitor Slurm jobs

Usage: myjobs [-h] [-v] [-p] [-r] [-j JOBID]

Options:

        -h | --help                             Display help
        -j | --jobid                            Jobid of the job to check
        -p | --pending                          Display information about all pending jobs
        -r | --running                          Display information about all running jobs
        -v | --version                          Display version of the script

[sfux@eu-login-35 ~]$ myjobs -j 2647208
Job information
 Job ID                          : 2647208
 Status                          : COMPLETED
 Running on node                 : eu-a2p-277
 User                            : sfux
 Shareholder group               : es_hpc
 Slurm partition (queue)         : normal.24h
 Command                         : sbatch --ntasks=4 --time=4:30:00 --mem-per-cpu=2g
 Working directory               : /cluster/home/sfux/testrun/adf/2021_test
Requested resources
 Requested runtime               : 04:30:00
 Requested cores (total)         : 4
 Requested nodes                 : 1
 Requested memory (total)        : 8192 MiB
Job history
 Submitted at                    : 2022-11-18T11:10:37
 Started at                      : 2022-11-18T11:10:37
 Queue waiting time              : 0 s
Resource usage
 Wall-clock                      : 00:16:38
 Total CPU time                  : 01:06:02
 CPU utilization                 : 99.24%
 Total resident memory           : 5573.25 MiB
 Resident memory utilization     : 68.03%

[sfux@eu-login-35 ~]$
```
